__author__ = 'Zhengyu Huang ,Yinuo Yao'
import numpy as np
import copy as copy
from scipy import sparse
from scipy.sparse.linalg import spsolve
import matplotlib.pyplot as plt
import Test
class Solver:
    """    An incompressible Navier-Stokes solver

                  Dirichlet
           -------------------------------                             y_ny         yc[ny+1]
          |                               |                                         yc[ny]
          |                               |                            y_ny-1
          |                               |  Dirichlet                 .            yc[ny-1
          |Dirichlet                      |                            .              .
          |   computational domain        |  or free boundary          .              .
          |                               |                            y_1            .
          |                               |                                         yc[1]
           ------------------------------                              y_0          yc[0]
                  Dirichlet

          x0         x1 ...                     x_nx
        xc[0] xc[1]      xc[2] ...              xc[nx+1]


    Data Members    ------------

    Lx, Ly: double, computation domain = [0,Lx][0,Ly]

    nx: int, number of grids in x direction

    ny: int, number of grids in y direction

    xx: double[nx+1], x coordinate of the faces of these grids

    yy: double[ny+1], y coordinate of the faces of these grids

    xc: double[nx+2], x coordinate of the cell centers,  xc[0] = 0  xc[-1]=Lx

    yc: double[nx+2], y coordinate of the cell centers,  yc[0] = 0  yc[-1]=Ly

    p: double[nx+2, ny+2], pressure at cell center, extend 1 ghost grids for each boundary

    u: double[nx+1, ny+2], u velocity at vertical faces, extend 1 ghost value at top and bottom

    v: double[nx+2, nx+1], v velocity at horizontal faces, extend 1 ghost value at left and right

    Au:double[nx+1, ny+2], save a_ij^u for pressure update purpose

    Av:double[nx+2, ny+1], save a_ij^u for pressure update purpose

    """
    def __init__(self, rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme, nx, ny, Lx, Ly,gridType,xx=0,yy=0, porousCol = -10, kappa = 1.0):
        self.rho = rho
        self.mu = mu
        self.nu = self.mu/self.rho
        self.kappa = kappa  #kappa/L
        self.porousCol = porousCol;

        self.t = 0.0
        self.dt = dt
        self.finalTime = finalTime
        self.maxIte = maxIte;

        self.outflowCondition = outflowCondition

        self.scheme = scheme


        if(gridType == 'Uniform'):
            self.uniformGridInit(nx,ny,Lx,Ly)
        elif(gridType == 'NonUniform'):
            self.nonuniformGridInit(xx,yy)

        self.case = case

        self.allocData()




    def uniformGridInit(self, nx,ny, Lx=10.0, Ly=1.0):
        '''
        Initialize solver,  uniform grid
        :param nx: number of grids in x direction
        :param ny: number of grids in y direction
        '''
        self.nx = nx
        self.ny = ny
        self.xx = np.linspace(0,Lx,nx+1)
        self.yy = np.linspace(0,Ly,ny+1)
        self.Lx, self.Ly = Lx, Ly

        dx,dy = Lx/nx, Ly/ny
        self.xc = np.linspace(-dx/2.0, Lx+dx/2.0, nx+2)
        self.xc[0], self.xc[-1] = 0,Lx
        self.yc = np.linspace(-dy/2.0, Ly+dy/2.0, ny+2)
        self.yc[0], self.yc[-1] = 0, Ly


    def nonuniformGridInit(self, xx, yy):
        '''

        :param xx:
        :param yy:
        :return:
        '''


        #xc/yc is the average of xx[i] and xx[i+1]/
        self.nx  = len(xx) - 1
        self.ny  = len(yy) - 1
        self.xx = xx
        self.yy = yy
        self.Lx, self.Ly = xx[-1], yy[-1]

        #Create faces,
        xc = np.zeros(len(xx)+1)
        yc = np.zeros(len(yy)+1)

        xc[0] ,yc[0] = xx[0],yy[0]
        for i in range(1,len(xx)):
            xc[i] = (xx[i-1] + xx[i])/2
            yc[i] = (yy[i-1] + yy[i])/2
        xc[-1] = xx[-1]
        yc[-1] = yy[-1]
        self.xc = xc
        self.yc = yc



    def initSolution(self):
        nx, ny = self.nx, self.ny
        if (self.case == 'Hagen-Poiseuille'):
            self.u[:,:] = 0
            self.u[0, 1:ny + 1] = 1.0
            self.u[nx, 1:ny + 1] = 1.0 if self.outflowCondition == 'Dirichlet' else self.u[nx - 1, 1:ny + 1]
            self.C[1, :] = 1.0
            self.v[:,:] = 0
            self.v[nx + 1, 1:ny] = 0 if self.outflowCondition == 'Dirichlet' else self.v[nx, 1:ny]

        elif (self.case == 'Taylor-Green'):
            '''
            From wiki https://en.wikipedia.org/wiki/Taylor%E2%80%93Green_vortex
            0 <= x,y <= 2 pi
            u(x,y,t) = cos(x)*sin(y) exp(-2 * nu * t)
            v(x,y,t) = -sin(x)*cos(y) exp(-2 * nu * t)
            p(x,y,t) = -rho/4 * (cos(2*x) + cos(2*y)) exp(-4 * nu * t)
            '''
            self.u,_,_ = Test.TaylorGreen(self.xx, self.yc , self.rho, self.nu, 0.0)
            _,self.v,_ = Test.TaylorGreen(self.xc, self.yy, self.rho, self.nu, 0.0)
            _,_,self.p = Test.TaylorGreen(self.xx, self.yy, self.rho, self.nu, 0.0)

        elif(self.case == 'Cavity'):
            # self.u[:,:] = 0

            # self.u[1:nx, ny+1] = 1.0

            # self.v[:,:] = 0

            # self.C[:, ny] = 0.0

            self.u[:,:] = 0

            self.v[nx + 1, 1:ny] = 1.0

            self.v[:,:] = 0

            self.C[:, ny] = 0.0


    def imposeDirichletBoundary(self,u,v,C):
        t = self.t
        nx,ny = self.nx, self.ny
        xx, yy, xc, yc = self.xx, self.yy, self.xc, self.yc
        if(u is not None):
            if(self.case == 'Hagen-Poiseuille'):
                u[0, 1:ny + 1] = 1.00
                u[nx, 1:ny + 1] = 1.0 if self.outflowCondition=='Dirichlet' else u[nx-1, 1:ny + 1]
                u[:, ny + 1] = 0
                u[:, 0] = 0
            elif(self.case == 'Taylor-Green'):
                assert (self.outflowCondition == 'Dirichlet')
                u[0, :],_,_ = Test.TaylorGreen(xx[0],yc,self.rho, self.nu, t)
                u[nx, :],_,_ = Test.TaylorGreen(xx[-1],yc,self.rho, self.nu, t)
                u[:, ny + 1],_,_ = Test.TaylorGreen(xx,yc[-1],self.rho, self.nu, t)
                u[:, 0],_,_ = Test.TaylorGreen(xx,yc[0],self.rho, self.nu, t)
            elif(self.case == 'Cavity'):
                assert (self.outflowCondition == 'Dirichlet')
                # u[1:nx, ny+1] = 1.0 #top
                # u[0, 1:ny + 1] = 0
                # u[nx, 1:ny + 1] = 0
                # u[:, 0] = 0

                u[1:nx, ny+1] = 0 #top
                u[0, 1:ny + 1] = 0
                u[nx, 1:ny + 1] = 0
                u[:, 0] = 0




        if(v is not None):
            if (self.case == 'Hagen-Poiseuille'):
                v[0, :] = 0
                v[nx + 1, 1:ny] = 0 if self.outflowCondition=='Dirichlet' else v[nx, 1:ny]
                v[:, ny] = 0
                v[:, 0] = 0
            elif (self.case == 'Taylor-Green'):
                assert(self.outflowCondition == 'Dirichlet')
                _,v[0, :],_ = Test.TaylorGreen(xc[0],yy,self.rho, self.nu, t)
                _,v[nx + 1, :],_ =  Test.TaylorGreen(xc[-1],yy,self.rho, self.nu, t)
                _,v[:, ny],_ = Test.TaylorGreen(xc,yy[-1],self.rho, self.nu, t)
                _,v[:, 0],_ = Test.TaylorGreen(xc,yy[0],self.rho, self.nu, t)
            elif(self.case == 'Cavity'):
                assert (self.outflowCondition == 'Dirichlet')
                # v[0, :] = 0
                # v[nx + 1, 1:ny] = 0
                # v[:, ny] = 0
                # v[:, 0] = 0

                v[0, :] = 0
                v[nx + 1, 1:ny] = 1.0
                v[:, ny] = 0
                v[:, 0] = 0

        if(C is not None):
            if(self.case == 'Hagen-Poiseuille'):

                C[0, :] = 1.0
                C[nx+1, :] = C[nx,:]
                C[:, 0 ] = C[:,1]
                C[:, ny+1] = C[:,ny]
            elif (self.case == 'Taylor-Green'):
                print('Not implemented')
            elif (self.case == 'Cavity'):
                # C[:, 0 ] = C[:, 1]
                # C[0, : ] = C[1, :] 
                # C[:, ny] = 1.0
                # C[nx+1, : ] = C[nx, :] 

                C[:, 0 ] = C[:, 1]
                C[0, : ] = C[1, :] 
                C[:, ny+1] = C[:,ny]
                C[nx, : ] = 1.0

    def allocData(self):

        nx = self.nx
        ny = self.ny

        self.p = np.zeros((nx+2,ny+2))
        self.C = np.zeros((nx+2,ny+2))
        self.u = np.zeros((nx+1,ny+2))
        self.v = np.zeros((nx+2,ny+1))
        self.Au = np.zeros((nx+1,ny+2))
        self.Av = np.zeros((nx+2,ny+1))

        self.initSolution( )


    def parabolic(self,y):
        return 1.0

        dp = self.dp
        Lx,Ly = self.Lx, self.Ly
        b = Ly/2.0
        return dp/Lx/self.mu*(2*b*y - y*y)


    def solveVelStar(self,uStar, vStar):
        '''
        :param porousCol: int, if porousCol < 0, there is no porous wall, otherwise u_{porousCol,:} is porous wall

        use u, v, p

        :return: uStar shape(nx+1,ny+2),
               : vStar shape(nx+2,ny+1)
        '''

        self.solveUStar(self.u, self.v, uStar)
        #print('uStar is ')
        #print(uStar)
        self.solveVStar(self.u, self.v, vStar)
        #print('vStar is ')
        #print(vStar)


    def iterativeSolveVelStar(self,uStar, vStar, maxIte = 2):

        self.solveUStar(self.u, self.v, uStar)
        self.solveVStar(self.u, self.v, vStar)
        ite = 1
        while ite <  maxIte:
            self.solveUStar(uStar, vStar, uStar)
            self.solveVStar(uStar, vStar, vStar)
            ite += 1


    def solveUStar(self, u, v,uStar):
        '''
        compute uStar, from u^{n} in self.u, and p^n in self.p, and use u and v to compute linearized matrix
        a_p, a_e, a_n, a_s

        solve for uStar, u*_ij i = 1,2...k-1,k,k+1,...nx-1, j = 1,2...ny
        reorder u11, u12,u13,...u(1,ny),u21, u22,u23,...u(2,ny) ... ... u(nx-1,1),u(nx-1,2),...u(nx-1,ny)
        u_ij has id (i-1)*ny + j-1
        '''
        scheme = self.scheme

        dt  =self.dt
        nx,ny = self.nx, self.ny
        xx,yy,xc,yc = self.xx, self.yy,self.xc, self.yc
        nu = self.nu
        porousCol = self.porousCol
        row, col, value = [],[],[] #jacobian matrix
        res = np.empty((nx-1)*ny)  #right hand side vector
        ###########################################################################################
        # Loop all the u momentum control volume
        ###########################################################################################
        for i in range(1,nx):
            for j in range(1,ny+1):
                #for cell of u_ij, upwind scheme
                #a_e u_i+1,j + a_w u_i-1,j + a_n u_i,j+1 + a_s u_i,j-1 + a_p u_ij = res

                dx = xc[i+1] - xc[i]
                dy = yy[j] - yy[j-1]
                if(scheme == 'UDS'):
                    a_e =  min(u[i+1,j],0.0)*dy -  nu*dy/(xx[i+1] - xx[i])
                    a_w = -max(u[i-1,j], 0.0)*dy - nu*dy/(xx[i] - xx[i-1])
                    a_n = min((v[i,j] + v[i+1,j])/2.0,0.0)*dx -   nu*dx/(yc[j+1] - yc[j])
                    a_s = -max((v[i,j-1] + v[i+1,j-1])/2.0,0.0)*dx - nu*dx/(yc[j] - yc[j-1])
                    a_p = dx*dy/dt + max(u[i,j],0.0)*dy - min(u[i,j],0.0)*dy + \
                          max((v[i,j] + v[i+1,j])/2.0,0.0)*dx  - min((v[i,j-1] + v[i+1,j-1])/2.0,0.0)*dx + \
                          nu*(dy/(xx[i+1] - xx[i]) + dy/(xx[i] - xx[i-1]) + dx/(yc[j] - yc[j-1]) + dx/(yc[j+1] - yc[j]))

                elif(scheme == 'CDS'):
                    a_e = 0.25 * (u[i,j]+u[i+1,j])*dy - nu*dy/(xx[i+1] - xx[i])
                    a_w = -(0.25 * (u[i-1,j]+u[i,j])*dy + nu*dy/(xx[i] - xx[i-1]))
                    a_n = 0.25 * (v[i,j]+v[i+1,j])*dx - nu*dx/(yc[j+1] - yc[j])
                    a_s = -(0.25 * (v[i,j-1]+v[i+1,j-1])*dx + nu*dx/(yc[j] - yc[j-1]))
                    a_p = dx*dy/dt + 0.25*(u[i,j]+u[i+1,j])*dy - 0.25*(u[i-1,j]+u[i,j])*dy + 0.25 * (v[i,j]+v[i+1,j])*dx \
                          - 0.25 * (v[i,j-1]+v[i+1,j-1])*dx + nu*dy/(xx[i+1] - xx[i]) + nu*dy/(xx[i] - xx[i-1]) \
                          + nu*dx/(yc[j+1] - yc[j]) + nu*dx/(yc[j] - yc[j-1])


                self.Au[i,j] = a_p

                rhs = dx*dy*self.u[i,j]/dt  - dy*(self.p[i+1,j] - self.p[i,j])/self.rho
                p_id, n_id, s_id, w_id, e_id = (i-1)*ny + j-1,(i-1)*ny + j,(i-1)*ny + j-2,(i-2)*ny + j-1,i*ny + j-1

                if(i == porousCol): # on the porous wall
                    row.append(p_id)
                    col.append(p_id)
                    value.append(1.0)
                    res[p_id] = self.kappa/self.mu * (self.p[porousCol,j] - self.p[porousCol+1,j])
                else:

                    res[p_id] = rhs

                    #diagonal entry
                    row.append(p_id)
                    col.append(p_id)
                    value.append(a_p)

                    #left side, Dirichlet boundary condition, u_0,j is constant
                    if(i != 1):
                        row.append(p_id)
                        col.append(w_id)
                        value.append(a_w)
                    else:
                        res[p_id] -= a_w*u[i-1,j]

                    #right side
                    if(i != nx -1):
                        row.append(p_id)
                        col.append(e_id)  # u_nx,j = u_nx-1,j
                        value.append(a_e)
                    else:
                        if(self.outflowCondition == 'Free'):
                            row.append(p_id)
                            col.append(p_id) #u_nx,j = u_nx-1,j
                            value.append(a_e)
                        elif(self.outflowCondition == 'Dirichlet'):
                            res[p_id] -= a_e * u[i+1,j]

                    #top, Dirichlet boundary condition, u_i,ny+1 is constant
                    if(j!= ny):
                        row.append(p_id)
                        col.append(n_id)
                        value.append(a_n)
                    else:
                        res[p_id] -= a_n*u[i,j+1]

                    #bottom Dirichlet boundary condition, u_i,0 is constant
                    if(j != 1):
                        row.append(p_id)
                        col.append(s_id)
                        value.append(a_s)
                    else:
                        res[p_id] -= a_s*u[i,j-1]



        jacobian = sparse.coo_matrix((value,(row,col)),shape=((nx-1)*ny,(nx-1)*ny)).tocsr()
        uStar[1:nx,1:ny+1] = np.reshape(spsolve(jacobian,res),(nx-1,ny))
        self.imposeDirichletBoundary(uStar,None,None)


    def solveVStar(self, u, v,vStar):
        '''
        compute vStar, from v^{n} in self.v, and p^n in self.p, and use u and v to compute linearized matrix
        a_p, a_e, a_n, a_s
        # solve for uStar, v*_ij i = 1,2...nx, j = 1,2...ny-1
        # reorder v11, v12,v13,...v(1,ny-1),v21, v22, v23,...v(2,ny-1) ... ... v(nx,1), v(nx,2),...v(nx,ny-1)
        # v_ij has id (i-1)*(ny-1) + j-1

        # solve for vStar, v*_ij i = 1,2...nx, j = 1,2...ny-1
        '''
        scheme = self.scheme
        dt  =self.dt
        nx,ny = self.nx, self.ny
        xx,yy,xc,yc = self.xx, self.yy,self.xc, self.yc
        nu = self.nu
        porousCol = self.porousCol
        row, col, value = [],[],[] # jacobian matrix
        res = np.empty(nx*(ny-1))  # right hand side vector
        ###########################################################
        # Loop all the velocity control volume
        ##########################################################
        for i in range(1,nx+1):
            for j in range(1,ny):
                # for cell of v_ij, upwind scheme
                # a_e v_i+1,j + a_w v_i-1,j + a_n v_i,j+1 + a_s v_i,j-1 + a_p v_ij = res
                dx = xx[i] - xx[i-1]
                dy = yc[j+1] - yc[j]

                if(scheme == 'UDS'):
                    # upwind scheme
                    a_e = (min((u[i, j + 1] + u[i, j]) / 2.0, 0.0) * dy - nu * dy / (xc[i + 1] - xc[i]) if i != porousCol else 0) #because v at porous wall is 0
                    a_w = (-max((u[i - 1, j + 1] + u[i - 1, j]) / 2.0, 0.0) * dy - nu * dy / (xc[i] - xc[i - 1]) if i != porousCol + 1 else 0)#because v at porous wall is 0

                    a_n = min(v[i, j + 1], 0.0) * dx - nu * dx / (yy[j + 1] - yy[j])
                    a_s = -max(v[i, j - 1], 0.0) * dx - nu * dx / (yy[j] - yy[j - 1])
                    a_p = dx * dy / dt + (max((u[i, j + 1] + u[i, j]) / 2.0, 0.0) * dy + nu * dy / (xc[i + 1] - xc[i]) if i != porousCol else 2 * nu * dy / dx) \
                          + (-min((u[i - 1, j + 1] + u[i - 1, j]) / 2.0, 0.0) * dy + nu * dy / (xc[i] - xc[i - 1]) if i != porousCol + 1 else 2 * nu * dy / dx) \
                          + max(v[i, j], 0.0) * dx + nu * dx / (yy[j + 1] - yy[j]) \
                          + (-min(v[i, j], 0.0) * dx + nu * dx / (yy[j] - yy[j - 1]))

                elif(scheme == 'CDS'):
                    a_e = (0.25 * (u[i,j]+u[i,j+1])*dy - nu*dy/(xc[i + 1] - xc[i]) if i != porousCol else 0)#because v at porous wall is 0
                    a_w = (-(0.25 * (u[i-1,j]+u[i-1,j+1])*dy + nu*dy/(xc[i] - xc[i - 1])) if i != porousCol+1 else 0)#because v at porous wall is 0
                    a_n = 0.25 * (v[i,j]+v[i,j+1])*dx - nu*dy/(yy[j+1] - yy[j])
                    a_s = -(0.25 * (v[i,j-1]+v[i,j])*dx + nu*dy/(yy[j] - yy[j - 1]))
                    a_p = dx*dy/dt + (0.25 * (u[i,j]+u[i,j+1])*dy + nu*dy/(xc[i + 1] - xc[i]) if i!= porousCol else 2 * nu * dy / dx) \
                          +(- 0.25 * (u[i-1,j]+u[i-1,j+1])*dy + nu*dy/(xc[i] - xc[i - 1]) if i!= porousCol+1 else 2 * nu * dy / dx)\
                          + 0.25 * (v[i,j]+v[i,j+1])*dx + nu*dy/(yc[j] - yc[j-1]) \
                          - 0.25 * (v[i,j-1]+v[i,j])*dx + nu*dy/(yc[j] - yc[j-1])

                self.Av[i,j] = a_p
                rhs = dx*dy*self.v[i,j]/dt  - dx*(self.p[i,j+1] - self.p[i,j])/self.rho

                p_id, n_id, s_id, w_id, e_id = (i-1)*(ny-1) + j-1,(i-1)*(ny-1) + j,(i-1)*(ny-1) + j-2,(i-2)*(ny-1) + j-1,i*(ny-1) + j-1

                res[p_id] = rhs

                row.append(p_id)
                col.append(p_id)
                value.append(a_p)
                #left side Dirichlet boundary condition, v_0,j is constant
                if(i != 1): #v_0,j = 0
                    row.append(p_id)
                    col.append(w_id)
                    value.append(a_w)
                else:
                    res[p_id] -= a_w*v[i-1,j]

                #right side
                if(i != nx):
                    row.append(p_id)
                    col.append(e_id) #v_nx+1,j = v_nx,j
                    value.append(a_e)
                else:
                    if (self.outflowCondition == 'Free'):
                        row.append(p_id)
                        col.append(p_id)  # v_nx+1,j = v_nx,j
                        value.append(a_e)
                    elif (self.outflowCondition == 'Dirichlet'):
                        res[p_id] -= a_e * v[i + 1, j]

                #top, Dirichlet boundary condition, v_i,ny is constant
                if(j != ny-1):
                    row.append(p_id)
                    col.append(n_id)
                    value.append(a_n)
                else:
                    res[p_id] -= a_n*v[i, j+1]

                #bottom, Dirichlet boundary condition, v_i,0 is constant
                if(j != 1):
                    row.append(p_id)
                    col.append(s_id)
                    value.append(a_s)
                else:
                    res[p_id] -= a_s*v[i,j-1]



        jacobian = sparse.coo_matrix((value, (row, col)), shape=(nx * (ny-1), nx* (ny-1))).tocsr()
        vStar[1:nx+1, 1:ny] = np.reshape(spsolve(jacobian, res),(nx, ny-1))

        # specify the boundary condition for vStar
        self.imposeDirichletBoundary(None, vStar,None)


    def solveDeltaP(self,uStar, vStar, deltaP):
        '''
        use member data self.Au, self.Av and uStar, vStar, to compute deltaP
        :param uStar: shape(nx+1,ny+2),
        :param vStar: shape(nx+2,ny+1)
        :param porousCol: int, if porousCol < 0, there is no porous wall, otherwise u_{porousCol,:} is porous wall
        :return: deltaP shape(nx+2,ny+2)

        '''

        nx, ny = self.nx, self.ny
        xx, yy, xc, yc = self.xx, self.yy, self.xc, self.yc
        Au, Av = self.Au , self.Av
        porousCol = self.porousCol
        row, col, value = [], [], []
        res = np.empty(nx  * ny)
        # reorder p11, p12,p13,...p(1,ny),p21, p22, p23,...p(2,ny) ... ... p(nx,1), p(nx,2),...p(nx,ny)
        ###############################################
        # Loop all cells
        #         p_i,j+1
        # p_i-1,j  p_i,j,   p_i+1,j
        #         p_i,j-1
        # left side
        # a_e p_i+1,j + a_w p_i-1,j + a_n p_i,j+1 + a_s p_i,j-1 + a_p p_ij = res
        ##################################################
        for i in range(1, nx+1):
            for j in range(1, ny + 1):

                p_id, n_id, s_id, w_id, e_id = (i - 1) * ny + j - 1, (i - 1) * ny + j, (i - 1) * ny + j - 2, (i - 2) * ny + j - 1, i * ny  + j - 1
                if(i == nx and j == ny):
                    #For the matrix is singular, we force p^'[nx,ny] = 0.0
                    row.append(p_id)
                    col.append(p_id)
                    value.append(1.0)
                    res[p_id] = 0.0
                    continue

                dx = xx[i] - xx[i - 1]
                dy = yy[j] - yy[j - 1]

                a_e , a_w, a_n , a_s, a_p = 0., 0., 0., 0., 0.
                # left side  -u^'_{i-1j} dy = (-dy)* (-dy) *( p_{i,j} - p_{i-1,j})/Au_{i-1,j}/rho
                if (i == 1): # u^'_0,j = 0
                    pass
                elif(i == porousCol + 1):
                    a_p += dy*self.kappa/self.mu
                    a_w -= dy*self.kappa/self.mu
                else:
                    a_p += dy * dy / self.rho/Au[i-1,j]
                    a_w -= dy * dy / self.rho/Au[i-1,j]

                # righ side  u^'_{ij} dy = (dy)* (-dy) *( p_{i+1,j} - p_{i,j})/Au_{i,j}/rho
                if (i == porousCol):
                    a_e -= dy* self.kappa/self.mu
                    a_p += dy* self.kappa/self.mu
                elif(i == nx): #u_nx,j^' = u_{nx-1,j}^' todo
                    if(self.outflowCondition == 'Free'):
                        a_w += dy * dy / self.rho / Au[i - 1, j]
                        a_p -= dy * dy / self.rho / Au[i - 1, j]
                    elif(self.outflowCondition == 'Dirichlet'):
                        pass

                else:
                    a_p += dy * dy / self.rho / Au[i, j]
                    a_e -= dy * dy / self.rho / Au[i, j]

                # bottom -v^'_{ij-1} dx = (-dx)* (-dx) *( p_{i,j} - p_{i,j-1})/Au_{i,j-1}/rho
                if (j != 1):
                    a_p += dx*dx/self.rho/Av[i,j-1]
                    a_s -= dx*dx/self.rho/Av[i,j-1]
                # top v^'_{ij-1} dx = (dx)* (-dx) *( p_{i,j+1} - p_{i,j})/Au_{i,j}/rho
                if (j != ny):
                    a_p += dx*dx/self.rho/Av[i,j]
                    a_n -= dx*dx/self.rho/Av[i,j]



                res[p_id] = (uStar[i-1,j] - uStar[i,j])*dy + (vStar[i,j-1] - vStar[i,j])*dx
                ######################################################
                # set up jacobian
                ###########################################################
                row.append(p_id)
                col.append(p_id)
                value.append(a_p)
                # left side
                if(i != 1):
                    row.append(p_id)
                    col.append(w_id)
                    value.append(a_w)


                # right side
                if(i != nx):
                    row.append(p_id)
                    col.append(e_id)
                    value.append(a_e)

                # top
                if(j != ny):
                    row.append(p_id)
                    col.append(n_id)
                    value.append(a_n)

                # bottom
                if(j != 1):
                    row.append(p_id)
                    col.append(s_id if j != 1 else p_id)
                    value.append(a_s)



        jacobian = sparse.coo_matrix((value, (row, col)), shape=(nx * ny , nx * ny )).tocsr()
        #jac = jacobian.toarray()
        #print(jac)
        #print('deltap jacobian is ')
        #print(jacobian.toarray())
        #print('deltap res is ')
        #print(res)
        deltaP[1:nx + 1, 1:ny+1] = np.reshape(spsolve(jacobian, res), (nx, ny))
        #print('deltaP[1:nx + 1, 1:ny+1]')
        #print(deltaP[1:nx + 1, 1:ny+1])


    def correct(self,uStar, vStar, deltaP,relaxation = 1.0):
        '''
        update self.u, self.v and self.p
        :param uStar: shape(nx+1,ny+2),
        :param vStar: shape(nx+2,ny+1)
        :param deltaP shape(nx+2,ny+2)
        :param porousCol: int, if porousCol < 0, there is no porous wall, otherwise u_{porousCol,:} is porous wall

        '''
        nx,ny = self.nx, self.ny
        xx,yy,xc,yc = self.xx, self.yy, self.xc, self.yc
        porousCol = self.porousCol
        self.u[1:nx, 1:ny+1] = uStar[1:nx,1:ny+1] - \
                               np.diff(yy) * np.diff(deltaP[1:nx+1,1:ny+1],axis = 0) /self.Au[1:nx,1:ny+1]/self.rho

        self.v[1:nx+1, 1:ny] = vStar[1:nx+1,1:ny] - \
                               np.reshape(np.diff(xx),(-1,1)) * np.diff(deltaP[1:nx+1,1:ny+1]) /self.Av[1:nx+1,1:ny]/self.rho


        #update pressure
        self.p[1:nx+1,1:ny+1] += relaxation*deltaP[1:nx+1,1:ny+1]



        #handle boundary condition
        #porous wall
        if(porousCol >= 0):
            self.u[porousCol, 1:ny+1] = uStar[porousCol, 1:ny+1] + \
                                        self.kappa/self.mu * (deltaP[porousCol,1:ny+1] - deltaP[porousCol+1,1:ny+1])


        self.imposeDirichletBoundary(self.u, self.v,None)


    def solveMassTransport(self):
        '''

        '''
        
        #print C
        C = self.C
        scheme = self.scheme
        dt  =self.dt
        Sc = 1.0
        D = self.nu/Sc
        u = self.u
        v = self.v
        k = 0.2
        nx,ny = self.nx, self.ny
        xx,yy,xc,yc = self.xx, self.yy,self.xc, self.yc
        porousCol = self.porousCol
        row, col, value = [],[],[] #jacobian matrix
        res = np.empty(nx  * ny)  #right hand side vector


        for i in range(1,nx+1):
            # print("i = %d " %(i))
            for j in range(1,ny+1):
                # print("j = %d " %(j))
                # print np.shape(u)
                dx = xx[i] - xx[i-1]
                dy = yy[j] - yy[j-1]
                if(scheme == 'UDS'):
                    a_e =  min(u[i,j],0.0)*dy -  D*dy/(xc[i+1] - xc[i])
                    a_w = -max(u[i-1,j], 0.0)*dy - D*dy/(xc[i] - xc[i-1])
                    a_n = min(v[i,j],0.0)*dx -   D*dx/(yc[j+1] - yc[j])
                    a_s = -max(v[i,j-1],0.0)*dx - D*dx/(yc[j] - yc[j-1])
                    a_p = dx*dy/dt + max(u[i,j],0.0)*dy - min(u[i-1,j],0.0)*dy + \
                          max(v[i,j] ,0.0)*dx  - min(v[i,j-1],0.0)*dx + \
                          D*(dy/(xc[i+1] - xc[i]) + dy/(xc[i] - xc[i-1]) + dx/(yc[j] - yc[j-1]) + dx/(yc[j+1] - yc[j]))

                elif(scheme == 'CDS'):
                    a_e =   dy * u[i,j]/2 - dy*D/(xc[i+1] - xc[i])
                    a_w = - dy * u[i-1,j]/2 - dy*D/(xc[i] - xc[i-1])
                    a_n =   dx * v[i,j]/2 - dx*D/(yc[j+1] - yc[j])
                    a_s = - dx * v[i,j-1]/2 - dx*D/(yc[j] - yc[j-1])
                    a_p =   dx*dy/dt + dy * u[i,j]/2 - dy * u[i-1,j]/2 + dx * v[i,j]/2 - dx * v[i,j-1]/2 + D*dy/(xc[i+1] - xc[i]) + D*dy/(xc[i] - xx[i-1]) + \
                            D*dx/(yc[j] - yc[j-1]) + D*dx/(yc[j+1] - yc[j])


                rhs = dx*dy/dt * self.C[i,j]

                p_id, n_id, s_id, w_id, e_id = (i - 1) * ny + j - 1, (i - 1) * ny + j, (i - 1) * ny + j - 2, (i - 2) * ny + j - 1, i * ny  + j - 1




               #print p_id
                res[p_id] = rhs

                #diagonal entry
                row.append(p_id)
                col.append(p_id)
                value.append(a_p)

                #left side,
                if(i == 1):
                    res[p_id] -= a_w * C[i-1,j]

                elif(i == porousCol+1):
                    row.append(p_id)
                    col.append(w_id)
                    value.append(-k*dy)
                    row.append(p_id)
                    col.append(p_id)
                    value.append(k*dy)

                else:
                    row.append(p_id)
                    col.append(w_id)
                    value.append(a_w)


                #right side
                if(i == nx):
                    res[p_id] -= a_e * C[i+1,j]

                elif(i == porousCol):
                    row.append(p_id)
                    col.append(e_id)
                    value.append(-k*dy)
                    row.append(p_id)
                    col.append(p_id)
                    value.append(k*dy)

                else:
                    row.append(p_id)
                    col.append(e_id)
                    value.append(a_e)


                #top, Dirichlet boundary condition, u_i,ny+1 is constant
                if(j!= ny):
                    row.append(p_id)
                    col.append(n_id)
                    value.append(a_n)
                else:
                    #res[p_id] -= 0
                    res[p_id] -= -a_n * C[i,j+1]

                #bottom Dirichlet boundary condition, u_i,0 is constant
                if(j != 1):
                    row.append(p_id)
                    col.append(s_id)
                    value.append(a_s)
                else:
                    #res[p_id] -= 0
                    res[p_id] -= -a_s * C[i,j-1]


        jacobian = sparse.coo_matrix((value, (row, col)), shape=(nx * ny , nx * ny )).tocsr()
        C[1:nx+1,1:ny+1] = np.reshape(spsolve(jacobian,res),(nx,ny))
        
        '''
        for i in range(1,nx+1):
            # print("i = %d " %(i))
            for j in range(1,ny+1):
                if(C[i,j] < 0):
                    C[i,j] = 0
        '''
        C[C < 0.0] = 0.0
        self.imposeDirichletBoundary(None,None,C)


    def solve(self):


        ite = 0
        nx,ny = self.nx, self.ny
        uStar = np.empty((nx+1,ny+2))
        vStar = np.empty((nx+2,ny+1))
        deltaP = np.zeros((nx+2,ny+2))\

        while self.t < self.finalTime and ite <= self.maxIte :
            self.t += self.dt
            ite += 1
            print("Step %d - time %f" %(ite, self.t))

            self.iterativeSolveVelStar(uStar,vStar)

            self.solveDeltaP(uStar, vStar,deltaP)

            self.correct(uStar,vStar,deltaP)

           # if(case == 'Hagen-Poiseuille'):
            self.solveMassTransport()

                #self.correctDensity()
            #print('self.u is ')
            #print(self.u)
            #print('self.v is ')
            #print(self.v)
            #print('self.p is ')
            #print(self.p)
            print("Delta P norm is  %f - conservation error is %f" % (np.linalg.norm(deltaP),  self.checkMassConservation()))
            self.checkMassConservation()


    def checkMassConservation(self):
        res = 0.0
        u, v, p = self.u, self.v, self.p
        nx, ny = self.nx, self.ny
        xx, yy = self.xx, self.yy
        for i in range(1, nx+1):
            for j in range(1, ny + 1):
                #if(i != self.porousCol):
                    res += abs((u[i,j] - u[i-1,j]) *(yy[j] - yy[j-1]) + (v[i,j] - v[i,j-1]) *(xx[i] - xx[i-1]))
                #else:
                #    res += abs(self.kappa/self.mu * (p[i, j] - p[i + 1, j]) - u[i,j])

        return res


    def plotResult(self):
        nx, ny = self.nx, self.ny
        xx, yy, xc, yc = self.xx, self.yy, self.xc, self.yc
        # u grid
        Y,X  = np.meshgrid(yc,xx)
        plt.figure(1)
        plt.contour(X,Y, self.u,20, linewidths=0.5, colors = 'k')
        plt.pcolormesh(X,Y,self.u, shading='gouraud', cmap =plt.get_cmap('rainbow'))
        plt.title('u')
        plt.colorbar()

        # v grid

        Y, X = np.meshgrid(yy,xc)
        plt.figure(2)
        plt.contour(X,Y, self.v,20, linewidths=0.5, colors = 'k')
        plt.pcolormesh(X,Y,self.v, shading='gouraud', cmap =plt.get_cmap('rainbow'))
        plt.title('v')
        plt.colorbar()
        # p grid
        Y, X = np.meshgrid(yc[1:ny+1],xc[1:nx+1])
        plt.figure(3)
        plt.contour(X,Y, self.p[1:nx+1,1:ny+1],20, linewidths=0.5, colors = 'k')
        plt.pcolormesh(X,  Y , self.p[1:nx+1,1:ny+1], shading='gouraud', cmap =plt.get_cmap('rainbow'))
        plt.title('Pressure')
        plt.colorbar()
        # C grid
        Y, X = np.meshgrid(yc[1:ny+1],xc[1:nx+1])
        plt.figure(4)
        plt.contour(X,Y, self.C[1:nx+1,1:ny+1],20, linewidths=0.5, colors = 'k')
        plt.pcolormesh(X,  Y , self.C[1:nx+1,1:ny+1], shading='gouraud', cmap =plt.get_cmap('rainbow'))
        #plt.pcolormesh(X,  Y , self.C[1:nx+1,1:ny+1], shading='gouraud')
        #plt.pcolormesh(X,  Y , self.C[1:nx+1,1:ny+1])
        plt.title('Concentration')
        plt.colorbar()
        plt.show()


    def checkPoiseuilleFlow(self):

        nx, ny = self.nx, self.ny
        xx, yy, xc, yc = self.xx, self.yy, self.xc, self.yc
        # plot u at x = Lx/2.0
        u,v,p = self.u , self.v, self.p
        plt.figure(1)
        nx_mid = next(a[0] for a in enumerate(xx) if a[1] >= self.Lx/2.0)
        
        plt.xlabel('u')
        plt.ylabel('y')
        plt.title('u at x = %f'%xx[nx_mid])
        #print np.max(u[nx_mid,:])
        # gaiaU = [1,0.84123,0.78871,0.73722,0.68717,0.23151,0.00332,-0.13641,-0.20581,-0.21090,-0.15662,-0.10150,-0.06434,-0.04775,-0.04192,-0.03717,0.0]
        # gaiaAX = [1.0,0.9766,0.9688,0.9609,0.9531,0.8516,0.7344,0.6172,0.5000,0.4531,0.2813,0.1719,0.1016,0.0703,0.0625,0.0547,0.0]
        # plt.plot(gaiaU,gaiaAX,'ro')
        # plt.legend(('Our Solution','Ghia et al.'))

        #hagen analytical
        #delP = 32*self.mu*self.Lx*1.0/(self.Ly**2)
        #delP = 0.23728

        #anaUy = 1/(4*self.mu) * delP * (self.Ly * yc - yc**2)

        #plt.plot(anaUy,yc,'-o')
        plt.plot(u[nx_mid,:],yc,'*-')
        #plt.legend(('Parabolic Analytical Solution','Our Solution'))

        # plot u at y = Ly/2.0
        plt.figure(2)
        ny_mid = next(a[0] for a in enumerate(yc) if a[1] >= self.Ly / 2.0)
        plt.plot(xx,u[:,ny_mid],'-*')
        plt.xlabel('x')
        plt.ylabel('u')
        plt.title('u at y = %f' % yc[ny_mid])

        # v grid
        plt.figure(3)
        ny_mid = next(a[0] for a in enumerate(yc) if a[1] >= self.Ly / 2.0)
        plt.plot(xc,v[:,ny_mid],'-*')
        plt.xlabel('x')
        plt.ylabel('v')
        plt.title('v at y = %f' % yc[ny_mid])
        # gaiaV = [0.0,-0.05906,-0.07391,-0.08864,-0.10313,-0.16914,-0.22445,-0.24533,0.05454,0.17527,0.17507,0.16077,0.12317,0.10890,0.10091,0.09233,0.0]
        # gaiaAY = [1.0,0.9688,0.9609,0.9531,0.9453,0.9063,0.8594,0.8047,0.50,0.2344,0.2266,0.1563,0.0938,0.0781,0.0703,0.0625,0.0]
        # plt.plot(gaiaAY,gaiaV,'ro')
        # plt.legend(('Our Solution','Ghia et al.'))

        plt.figure(4)
        nx_mid = next(a[0] for a in enumerate(xx) if a[1] >= self.Lx/2.0)
        plt.plot(v[nx_mid,:],yy,'-*')
        plt.xlabel('v')
        plt.ylabel('y')
        plt.title('v at x = %f'%xx[nx_mid])

        # plot p at y = Ly/2.0
        plt.figure(5)
        ny_mid = next(a[0] for a in enumerate(yc) if a[1] >= self.Ly/2.0)
        plt.plot(xc[1:-1],  p[1:nx+1,ny_mid],'-*')
        plt.title('P at y = %f'%yc[ny_mid])
        plt.xlabel('x')
        plt.ylabel('P')
        plt.show()


#Not in the class
def calculateAccuracy(rho, mu, finalTime, maxIte, setT, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType):
    #uold = np.zeros((nx+1,ny+2))
    errorSetTime = np.zeros(len(setT))
    for i in range(len(setT)):
        pipeFlowAccuracy = Solver(rho, mu, finalTime, maxIte, setT[i], outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType)
        pipeFlowAccuracy.solve()
        nx_mid = next(a[0] for a in enumerate(pipeFlowAccuracy.xx) if a[1] >= pipeFlowAccuracy.Lx/2.0)
        delP = 0.23728
        anaUy = 1/(4*pipeFlowAccuracy.mu) * delP * (pipeFlowAccuracy.Ly * pipeFlowAccuracy.yc - pipeFlowAccuracy.yc**2)
        print len(pipeFlowAccuracy.u[nx_mid,:])
        print len(anaUy)
        errorSetTime[i] = np.linalg.norm(pipeFlowAccuracy.u[nx_mid,:] - anaUy,2)
        #uold = pipeFlowAccuracy.u
    return errorSetTime

def calculateGlobalAccuracy(rho, mu, finalTime, maxIte, setT, outflowCondition, case, scheme,  setX, setY, Lx, Ly,gridType):
    errorSet = np.zeros(len(setT))
    for i in range(len(setT)):
        uold = np.zeros((setX[i]+1,setY[i]+2))
        pipeFlowAccuracy = Solver(rho, mu, finalTime, maxIte, setT[i], outflowCondition, case, scheme,  setX[i], setY[i], Lx, Ly,gridType)
        pipeFlowAccuracy.solve()
        errorSet[i] = np.linalg.norm(pipeFlowAccuracy.u - uold,'fro')
        uold = pipeFlowAccuracy.u
    return errorSet

def plotAccuracy(xSet,errorSet):
    order1 = np.multiply(1,xSet);
    # order2 = np.multiply(0.00000001,np.multiply(set,set))
    plt.loglog(xSet[0:-1],order1[0:-1])
    # plt.loglog(xSet[0:-1],order2[0:-1])        
    plt.loglog(xSet[0:-1],errorSet[1:])
    #  plt.legend(('O(1)','O(2)','CFD'))
    plt.legend(('O(1)','CFD'))
    plt.title('Accuracy')
    plt.show()

if __name__ == "__main__":


    case = 'Hagen-Poiseuille'

    gridType = 'Uniform' # Uniform/ NonUniform
    testType = 'Solve' #Accuracy/Solve

    if(case == 'Taylor-Green'):
        ######################################
        # Test case Taylor Green Vorticity
        ######################################

        rho = 1.0
        mu = 0.05
        dt = 0.01
        finalTime = 2.0
        maxIte = 1000;
        outflowCondition = 'Dirichlet'
        scheme = 'CDS'
        nx, ny = 20, 20
        Lx, Ly = 2*np.pi, 2*np.pi

        # if(gridType =='Uniform'):
        #     vorticitySolver = Solver(rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType)
        #     vorticitySolver.solve()
        #     vorticitySolver.plotResult()
        # elif(gridType == 'NonUniform'):
        #     xx = np.geomspace(0.01,Lx,nx)
        #     xx = np.insert(xx,0,0.0)

        if (testType == 'Solve'):
            rho = 1.0
            mu = 0.05
            dt = 0.01
            finalTime = 1.0
            maxIte = 10000;
            outflowCondition = 'Dirichlet'
            scheme = 'UDS'
            nx, ny = 20, 20
            Lx, Ly = 2*np.pi, 2*np.pi

            if(gridType =='Uniform'):
                vorticitySolver = Solver(rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType)
                vorticitySolver.solve()
                #vorticitySolver.plotResult()
            elif(gridType == 'NonUniform'):
                xx = np.geomspace(0.01,Lx,nx)
                xx = np.insert(xx,0,0.0)
                yy = np.geomspace(0.01,Ly,ny)
                yy = np.insert(yy,0,0.0)
                vorticitySolver = Solver(rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType,xx,yy)
                vorticitySolver.solve()
                vorticitySolver.plotResult()

            xx, yy, xc, yc = vorticitySolver.xx, vorticitySolver.yy, vorticitySolver.xc, vorticitySolver.yc
            rho, nu, t = vorticitySolver.rho, vorticitySolver.nu, vorticitySolver.t
            exactU, _, _ = Test.TaylorGreen(xx, yc, rho, nu, t)
            _, exactV, _ = Test.TaylorGreen(xc, yy, rho, nu, t)
            _, _, exactP = Test.TaylorGreen(xx, yy, rho, nu, t)
            print('At time %f, dt is %f, nx is %d, ny is %d', t, dt, nx, ny)
            print('Relative Frobenius norm error of U , V are %f , %f', np.linalg.norm(exactU - vorticitySolver.u)/np.linalg.norm(exactU),
                  np.linalg.norm(exactV - vorticitySolver.v)/np.linalg.norm(exactV))
        elif(testType == 'Accuracy'):
            rho = 1.0
            mu = 0.05
            dt = 0.05/2
            finalTime = 2.0
            maxIte = 1000;
            outflowCondition = 'Dirichlet'
            scheme = 'UDS'
            nx, ny = 30,30
            Lx, Ly = 2*np.pi, 2*np.pi
            # setT = [0.2,0.1,0.05,0.025,0.01250,0.00625]
            # setX = [10,20,30,40,50,60]
            # setY = [10,20,30,40,50,60]
            setT = [0.4,0.2,0.1,0.05]
            setX = [10,20,30,40]
            setY = [10,20,30,40]

            
            errorSetGlobal = calculateGlobalAccuracy(rho, mu, finalTime, maxIte, setT, outflowCondition, case, scheme,  setX, setY, Lx, Ly,gridType)
           # errorSetTime = calculateAccuracy(rho, mu, finalTime, maxIte, setT, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType)

            order1 = np.multiply(1,setT);
            plt.loglog(setT[0:-1],order1[0:-1])
           # plt.loglog(setT[0:-1],errorSetTime[1:],'D-')  
            #print errorSetTime     
            plt.loglog(setT[0:-1],errorSetGlobal[1:],'*-')
            plt.legend(('O(1)','CFD'))
            plt.title('Accuracy')
            plt.show()

    elif(case == 'Cavity'):
        #############################
        # Test case Hagen-Poiseuille
        #############################
        rho = 1.0
        mu = 0.01
        dt = 0.005
        finalTime = 100.0
        maxIte = 1000;
        outflowCondition = 'Dirichlet'
        scheme = 'CDS'
        #nx,ny = 32,32
        #nx,ny = 10,10
        nx,ny = 30,30
        Lx,Ly = 1.0,1.0
        pipeFlowSolver = Solver(rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme, nx, ny, Lx, Ly,gridType)
        pipeFlowSolver.solve()
        pipeFlowSolver.plotResult()
        pipeFlowSolver.checkPoiseuilleFlow()
    elif(case == 'Hagen-Poiseuille'):
        #############################
        # Test case Hagen-Poiseuille
        #############################
        if(testType == 'Solve'):
            rho = 1.0
            mu = 0.01
            dt = 0.01
            finalTime = 15.0
            maxIte = 1000
            outflowCondition = 'Free'
            scheme = 'CDS'
            nx,ny = 30,30
            Lx,Ly = 10.0,1.0
            pipeFlowSolver = Solver(rho, mu, finalTime, maxIte, dt, outflowCondition, case,scheme,  nx, ny, Lx, Ly,gridType, None, None, porousCol=nx//2,kappa=0.1 )

            # pipeFlowSolver.u = np.load('u99.npy')
            # pipeFlowSolver.v = np.load('v99.npy')
            # pipeFlowSolver.p = np.load('p99.npy')
            # pipeFlowSolver.C = np.load('C99.npy')


            pipeFlowSolver.solve()
            #pipeFlowSolver.checkPoiseuilleFlow()

            # np.save('u99', pipeFlowSolver.u)
            # np.save('v99', pipeFlowSolver.v)
            # np.save('p99', pipeFlowSolver.p)
            # np.save('C99', pipeFlowSolver.C)

            pipeFlowSolver.plotResult()
            # pipeFlowSolver.checkPoiseuilleFlow()

            # plt.plot(pipeFlowSolver.xx,pipeFlowSolver.C[0,1:])
            # plt.plot(pipeFlowSolver.xx,pipeFlowSolver.C[1,1:])
            # plt.plot(pipeFlowSolver.xx,pipeFlowSolver.C[2,1:])
            # plt.plot(pipeFlowSolver.xx,pipeFlowSolver.C[3,1:])
            # plt.legend(('y0','y1','y2','y3'))
            # plt.show()
            # print pipeFlowSolver.C
            # print pipeFlowSolver.C.shape
            # print pipeFlowSolver.xx.shape
            # print type(pipeFlowSolver.C)
            # print pipeFlowSolver.C[1,1:].shape
            
            # print(pipeFlowSolver.v[pipeFlowSolver.porousCol-1,:])
            # print(pipeFlowSolver.v[pipeFlowSolver.porousCol,:])
            # print(pipeFlowSolver.v[pipeFlowSolver.porousCol+1,:])
            # print(pipeFlowSolver.v[pipeFlowSolver.porousCol+2,:])

        elif(testType == 'Accuracy'):
            rho = 1.0
            mu = 0.01
            finalTime = 1.0
            maxIte = 1000
            outflowCondition = 'Free'
            scheme = 'CDS'
            nx,ny = 30,30
            #ny = 30
            #dt = 0.01
            Lx,Ly = 10.0,1.0
            set = [0.01,0.005,0.0025]#,0.0025,0.00125]
            #set = [15,30,60]#,120]
            errorType = 'time'
            #Error converges then increase....
            if(gridType =='Uniform'):
                errorSet = calculateAccuracy(rho, mu, finalTime, maxIte, set, outflowCondition, case, scheme,  nx, ny, Lx, Ly,gridType)
               #errorSet = calculateAccuracy(rho, mu, finalTime, maxIte, dt, outflowCondition, case, scheme,  set, ny, Lx, Ly,gridType)
                plotAccuracy(set,errorSet)

#frsd
