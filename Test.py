import numpy as np
def TaylorGreen(x,y,rho, nu, t):
    '''
    From wiki https://en.wikipedia.org/wiki/Taylor%E2%80%93Green_vortex
    0 <= x,y <= 2 pi
    u(x,y,t) = cos(x)*sin(y) exp(-2 * nu * t)
    v(x,y,t) = -sin(x)*cos(y) exp(-2 * nu * t)
    p(x,y,t) = -rho/4 * (cos(2*x) + cos(2*y)) exp(-4 * nu * t)
    '''
    if(isinstance(x, np.ndarray) and isinstance(y, np.ndarray)):
        yy,xx = np.meshgrid(y,x)
    else:
        yy,xx = y,x
    u = np.cos(xx)*np.sin(yy)*np.exp(-2*nu*t)
    v = -np.sin(xx)*np.cos(yy)*np.exp(-2*nu*t)
    p = -rho/4.0 *(np.cos(2*xx) + np.cos(2*yy))*np.exp(-4*nu*t)

    return u,v,p